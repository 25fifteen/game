<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsQuests extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		/* Table used for quest-tracking */ 
		Schema::create('teams_quests', function($table){

			$table->integer('team_id')->unsigned();
			$table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade')->onUpdate('cascade');
			
			$table->integer('quest_id')->unsigned();
			$table->foreign('quest_id')->references('id')->on('quests')->onDelete('cascade')->onUpdate('cascade');
			
			$table->boolean('complete')->default(0);

			/* Max gold = quests.reward_gold */
			$table->integer('collected_gold');

			$table->boolean('collected_hero')->default(0);
			

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
