<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerHeroes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('player_heroes', function($table){

			$table->integer('player_id')->unsigned();
			$table->foreign('player_id')->references('id')->on('players')->onDelete('cascade')->onUpdate('cascade');

			/* Max 20 hero_id per player_id*/
			$table->integer('hero_id')->unsigned();
			$table->foreign('hero_id')->references('id')->on('heroes')->onDelete('cascade')->onUpdate('cascade');
			
			$table->integer('skill_id')->unsigned();
			$table->foreign('skill_id')->references('id')->on('skills')->onDelete('cascade')->onUpdate('cascade');

			$table->integer('current_base_attack');
			$table->integer('current_weapon_damage');
			$table->integer('current_defence');
			$table->integer('current_defence_per_shield');
			$table->integer('current_health');
			$table->integer('current_regeneration');
			$table->integer('current_health_per_potion');
			$table->integer('current_blunting');
			$table->integer('current_poison');

			$table->double('current_armor_durability');
			$table->double('current_gold_boost');
			$table->double('current_leech');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_heroes');
	}

}
