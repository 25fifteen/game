<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeams extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('teams', function($table){
			$table->increments('id');
			$table->text('name');

			/* Maximum of "player.team_slots" records with the same player_id */
			$table->integer('player_id')->unsigned();
			$table->foreign('player_id')->references('id')->on('players')->onDelete('cascade')->onUpdate('cascade');
			
			$table->integer('slots')->default(4)->unsigned(); /* Default 4 hero-slots. Decrease as the player adds records in the heroes_teams-table. 
																Cant go lower than 0 slots left, hence unsigned.*/
			$table->integer('total_base_attack');
			$table->integer('total_weapon_damage');
			$table->integer('total_defence');
			$table->integer('total_defence_per_shield');
			$table->integer('total_health');
			$table->integer('total_regeneration');
			$table->integer('total_health_per_potion');
			$table->integer('total_blunting');

			$table->integer('total_armor_durability');
			$table->integer('total_gold_boost');
			$table->integer('total_leech');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('teams');
	}

}
