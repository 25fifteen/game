<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuests extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('quests', function($table){
			$table->increments('id');

			$table->integer('required_stamina');
			$table->integer('difficulty_id')->unsigned();
			$table->foreign('difficulty_id')->references('id')->on('difficulties')->onDelete('cascade')->onUpdate('cascade');
			
			$table->integer('reward_gold');
			$table->integer('reward_premium_currency');
			
			/* If there's no hero reward reward_hero_id will be null. Might be better to use 0 instead of null. */
			$table->integer('reward_hero_id')->unsigned()->nullable()->default(null);
			$table->foreign('reward_hero_id')->references('id')->on('heroes')->onDelete('cascade')->onUpdate('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('quests');
	}

}
