<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeroesTeams extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('heroes_teams', function($table){
			
			/* Max 4 hero_id per team_id*/
			$table->integer('hero_id')->unsigned();
			$table->foreign('hero_id')->references('id')->on('heroes')->onDelete('cascade')->onUpdate('cascade');
			
			$table->integer('team_id')->unsigned();
			$table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('heroes_teams');
	}

}
