<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeroes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('heroes', function($table){
			$table->increments('id');

			$table->text('name');

			$table->integer('base_attack');
			$table->integer('weapon_damage');
			$table->integer('defence');
			$table->integer('defence_per_shield');
			$table->integer('health');
			$table->integer('regeneration');
			$table->integer('health_per_potion');
			$table->integer('blunting');
			$table->integer('poison');

			$table->double('armor_durability');
			$table->double('gold_boost');
			$table->double('leech');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('heroes');
	}

}
