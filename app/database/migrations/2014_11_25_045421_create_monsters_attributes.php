<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonstersAttributes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('monsters_attributes', function($table){
			$table->integer('monster_id')->unsigned();
			$table->foreign('monster_id')->references('id')->on('monsters')->onDelete('cascade')->onUpdate('cascade');
			
			$table->integer('attribute_id')->unsigned();
			$table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade')->onUpdate('cascade');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('monsters_attributes');
	}

}
