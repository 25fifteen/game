<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('players', function($table){
			$table->increments('id');

			$table->text('email');
			$table->text('code');
			$table->text('name');

			$table->integer('stamina')->default(10);
			$table->integer('premium_currency')->default(0);
			$table->integer('gold')->default(0);
			$table->integer('hero_slots')->default(10);
			$table->integer('team_slots')->default(5);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('players');
	}

}
